# youcallme

Tool to callback from inside a gitlab runner to your local dev machine via ngrok

### Latest release

https://gitlab.com/idawson/youcallme/-/packages

## How to Use

Check the [video how to](https://www.youtube.com/watch?v=KRmRiQoq6_M).

- Grab a copy of ngrok from [ngrok.com](https://ngrok.com/).
- Start a ngrok server on your development machine: `ngrok http 9999`
- Clone this repo and start it locally: `./youcallme`
- Use https://gitlab.com/idawson/youcallme-demo/-/blob/main/.gitlab-ci.yml as a template to add in to the job that is causing you problems.
- Don't forget to replace the `CALLBACK_SERVER` variable's value with the one that is generated for your ngrok instance.
- Start your job
- Watch it connect into your development machine!
- Don't forget to cancel your running debug job as the server will potentially listen forever.

With terminal support added, commands such as vim should work, as well as tab completion!

```
$ ./youcallme
/builds/idawson/youcallme-demo # ls
README.md  youcallme
/builds/idawson/youcallme-demo # id
uid=0(root) gid=0(root) groups=0(root),1(bin),2(daemon),3(sys),4(adm),6(disk),10(wheel),11(floppy),20(dialout),26(tape),27(video)
/builds/idawson/youcallme-demo # hostname
runner-fa6cab46-project-30676434-concurrent-0
/builds/idawson/youcallme-demo #
```

## TODO

Terminal support works now, so if you have a feature request create an issue!
